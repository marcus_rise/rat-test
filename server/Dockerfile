ARG NODE_VERSION=14

FROM node:$NODE_VERSION AS dev
WORKDIR /app

USER node

CMD npm install --quiet && npm run start

EXPOSE 7421

FROM node:${NODE_VERSION}-stretch-slim AS build
WORKDIR /app

COPY package*.json ./

RUN npm ci

COPY ./src ./src
COPY ./*.js* ./

RUN npm run build
RUN npm prune --production && npm cache clear --force

FROM node:${NODE_VERSION}-stretch-slim as prod
WORKDIR /app

ENV NODE_ENV=production

COPY --from=build /app/package*.json ./
COPY --from=build /app/node_modules ./node_modules
COPY --from=build /app/dist ./dist

CMD node dist/index.js

EXPOSE 7421

