import React from "react";
import Form from "react-bootstrap/Form";

interface IProps<T> {
    value: T | null;
    items: T[];
    id: string;
    label?: string;
    change: (val: T) => void;
    noSelectedLabel: string;
}

export const Select: React.FC<IProps<string>> = (props) => {
    return <Form>
        <Form.Group controlId={props.id}>
            {props.label && <Form.Label>{props.label}</Form.Label>}
            <Form.Control
                as="select"
                custom
                onChange={(e) => props.change(e.target.value)}
            >
                <option value={""}>{props.noSelectedLabel}</option>
                {props.items.map(i => <option key={i}>{i}</option>)}
            </Form.Control>
        </Form.Group>
    </Form>
}
