import {IRatService} from "./rat.service-interface";
import {IRat} from "./rat.interface";

export class RatService implements IRatService {
    private readonly baseUrl: string = "/api";

    async getItem(name: string): Promise<IRat | null> {
        let res = null;

        await fetch(this.baseUrl + "/rat/" + name)
            .then((res) => res.json())
            .then((data) => {
                res = data;
            })
            .catch(console.error);

        return res;
    }

    async getList(): Promise<string[]> {
        let items: string[] = [];

        await fetch(this.baseUrl + "/rat-names")
            .then((res) => res.json())
            .then((data) => {
                items = data;
            })
            .catch(console.error);

        return items;
    }
}
