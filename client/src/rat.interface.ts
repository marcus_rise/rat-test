export interface IRat {
    width: number;
    height: number;
    nickname?: string;
}
