import {IRat} from "./rat.interface";

export interface IRatService {
    getList(): Promise<string[]>;

    getItem(name: string): Promise<IRat | null>
}

export const RAT_SERVICE_PROVIDER = Symbol("IRatService");
