import {RAT_SERVICE_PROVIDER} from "./rat.service-interface";
import {RatService} from "./rat.service";

export const useInject = <T>(key: Symbol | string): T => {
    let res: T;

    switch (key) {
        case RAT_SERVICE_PROVIDER:
            res = new RatService() as unknown as T;
            break;
        default:
            res = null as unknown as T;
    }

    return res;
}
