import {RatService} from "./rat.service";
import {rest} from "msw";
import {setupServer} from "msw/node";
import {IRat} from "./rat.interface";

describe("RatService", () => {
    let service: RatService;

    beforeEach(() => {
        service = new RatService();
    });

    describe("getList", () => {
        test("ok", async () => {
            const handles = [rest.get("/api/rat-names", (req, res, context) => {
              return res(context.json(["", ""]));
            })];
            const server = setupServer(...handles);
            server.listen();

            const list = await service.getList();

            expect(list).toHaveLength(2);

            server.close();
        });

        test("error", async () => {
            const handles = [rest.get("/api/rat-names", (req, res, context) => {
              return res(context.status(500));
            })];
            const server = setupServer(...handles);
            server.listen();

            const list = await service.getList();

            expect(list).toHaveLength(0);

            server.close();
        });
    });

    describe("getItem", () => {
        test("ok", async () => {
            const handles = [rest.get("/api/rat/:name", (req, res, context) => {
                return res(context.json(<IRat>{
                    nickname: req.params.name,
                    height: 1,
                    width: 2,
                }));
            })];
            const server = setupServer(...handles);
            server.listen();

            const item = await service.getItem("test");

            expect(item).not.toBeNull();
            expect(item?.nickname).toEqual("test");
            expect(item?.height).toEqual(1);
            expect(item?.width).toEqual(2);

            server.close();
        });

        test("error", async () => {
            const handles = [rest.get("/api/rat/:name", (req, res, context) => {
                return res(context.status(500));
            })];
            const server = setupServer(...handles);
            server.listen();

            const item = await service.getItem("test");

            expect(item).toBeNull();

            server.close();
        });
    })
});
