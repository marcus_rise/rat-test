import React, {useCallback, useEffect, useState} from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import {IRatService, RAT_SERVICE_PROVIDER} from "./rat.service-interface";
import {useInject} from "./use-inject.hook";
import {Select} from "./Select";
import Col from "react-bootstrap/Col";
import {IRat} from "./rat.interface";
import {RatCard} from "./RatCard";

const App: React.FC = () => {
    const [rat, setRat] = useState<IRat | null>(null);
    const [rats, setRats] = useState<string[]>([]);
    const service = useInject<IRatService>(RAT_SERVICE_PROVIDER);

    const initData = useCallback(() => {
        service.getList()
            .then((data) => {
                setRats(data);
            });
    }, [service]);

    useEffect(initData, [])

    const selectRat = (rat?: string): void => {
        if (rat) {
            service.getItem(rat)
                .then((data) => {
                    setRat(data);
                })
        } else {
            setRat(null);
        }
    }

    return (
        <main>
            <Container className="d-flex justify-content-center align-items-center flex-column"
                       style={{height: "100vh"}}>
                <Row>
                    <Col xs={12}>
                        <h1
                            style={{fontSize: "2rem", textAlign: "center"}}
                        >List of RATS that should stay away <br/> from my DAMN BINS</h1>
                    </Col>
                </Row>
                <Row className="d-flex justify-content-center align-items-center">
                    <Col>
                        <Select
                            value={rat?.nickname ?? ""}
                            items={rats}
                            id={"rats"}
                            change={selectRat}
                            noSelectedLabel={"No Rat"}
                        />
                    </Col>
                </Row>
                {rat &&
                <Row className="d-flex justify-content-center align-items-center">
                    <Col xs={12}>
                        <RatCard rat={rat} noNameLabel={"Uncool Rat with no Nickname"} />
                    </Col>
                </Row>
                }
            </Container>
        </main>
    );
};

export default App;
