import {useInject} from "./use-inject.hook";
import {RAT_SERVICE_PROVIDER} from "./rat.service-interface";

describe("useInject", () => {
    test("IRatService", () => {
        const service = useInject(RAT_SERVICE_PROVIDER);

        expect(service).not.toBeNull();
    });

    test("unknown service", () => {
        const service = useInject("test");

        expect(service).toBeNull();
    });
});
