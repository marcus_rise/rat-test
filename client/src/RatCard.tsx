import React from "react";
import {IRat} from "./rat.interface";
import Card from "react-bootstrap/Card";

interface IProps {
    rat: IRat
    noNameLabel: string;
}

export const RatCard: React.FC<IProps> = ({rat, noNameLabel}) => {
    return <Card style={{width: "300px", textAlign: "center"}}>
        <Card.Body>
            <Card.Title>
                <h2 style={{fontSize: "1.5rem"}}>{rat.nickname || noNameLabel}</h2>
            </Card.Title>
            <Card.Text style={{fontSize: "1.1rem"}}>
                <b>Width: </b><span>{rat.width}</span><br/>
                <b>Height: </b><span>{rat.height}</span>
            </Card.Text>
        </Card.Body>
    </Card>;
};
